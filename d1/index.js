let http = require("http");
//simulating a database
let courses = [
	{
		name: "English 101",
		price: 23000,
		isActive: true
	},
	{
		name: "Math 101",
		price: 25000,
		isActive: true
	},
	{
		name: "Reading 101",
		price: 21000,
		isActive: true
	}
];

http.createServer(function(req,res){

	console.log(req.url);
	console.log(req.method);

	if(req.url === "/" && req.method === "GET"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a GET method request");

	} else if(req.url === "/" && req.method === "POST"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a GET method request");

	} else if(req.url === "/" && req.method === "PUT"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a PUT method request");

	} else if(req.url === "/" && req.method === "DELETE"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a DELETE method request");

	}  else if(req.url === "/courses" && req.method === "GET"){
		res.writeHead(200,{'Content-Type':'application/JSON'});
		res.end(JSON.stringify(courses));

	} else if(req.url === "/courses" && req.method === "POST"){

		let requestBody = "";

		req.on('data',function(data){

			//console.log(data);
			requestBody += data
		})

		req.on('end',function(){
			console.log(requestBody);

			requestBody = JSON.parse(requestBody);
			let newCourse = {
				name: requestBody.name,
				price: requestBody.price,
				isActive: true
			}

			console.log(newCourse);
			courses.push(newCourse);//add a course in the array
			console.log(courses);

		res.writeHead(200,{'Content-Type':'application/json'});
		res.end(JSON.stringify(courses));
		})

		

		//res.end(`${req.method}`);// can put template literals in res. end// result in post man is POST.
	} 


}).listen(4000);

console.log("Server is running at localhost:4000");